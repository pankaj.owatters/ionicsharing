                  Sharing(via sms or other social sharing)
               ____________________________________________________

Task :-
     As today's task was to creating an app that'll have a button by clicking on which the user 
     will have option of sharing via various media like sms, email, whatsapp, facebook etc. 

Condition:-
     This app can only be used in mobile device like android or ios.

About the app:-
      1. At the very start i created a blank ionic app 
      2. and for using socialsharing, you have to add SocialSharing plugin for that add:
           i). ionic cordova plugin add cordova-plugin-x-socialsharing
           ii). npm install --save @ionic-native/social-sharing
      3. now you have to import {SocialSharing} into app.module.ts
      4. now inject a constructor to use this SocialSharing as:
                 constructor(private socialSharing: Socialsharing){}
      5. now inside a function(i.e. bind to a click event with the button in your home.component.html) write the following command :
                 this.socialSharing.share(this.name)  <== here this.name is referring to the name(string) that will send as a parameter
      6. in this perticular project, i also used name as an input to user that is bind to the name(of type string) of component.ts through [(ngmodel)].                                  